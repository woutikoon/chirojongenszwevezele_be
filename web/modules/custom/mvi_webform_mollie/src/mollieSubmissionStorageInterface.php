<?php

namespace Drupal\mvi_webform_mollie;

/**
 * Interface mollieSubmissionStorageInterface
 *
 * @package Drupal\mvi_webform_mollie
 */
interface mollieSubmissionStorageInterface {

    /**
     * @param $sid
     * @return mixed
     */
    public function getMollieStatus($sid);

    /**
     * @param $sid
     * @param $status
     * @return mixed
     */
    public function setMollieStatus($sid, $status);

    /**
     * @param $sid
     * @return mixed
     */
    public function getMollieRemoteId($sid);

    /**
     * @param $sid
     * @return mixed
     */
    public function loadByRemoteMollieId($sid);

    /**
     * @param $sid
     * @param $status
     * @return mixed
     */
    public function setMollieRemoteId($sid, $remote_id);

}
