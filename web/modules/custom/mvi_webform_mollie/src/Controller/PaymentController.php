<?php

namespace Drupal\mvi_webform_mollie\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\mvi_webform_mollie\MollieSubmissionStorage;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Mollie\Api\Types\PaymentStatus as MolliePaymentStatus;

/**
 * Class PaymentController.
 */
class PaymentController extends ControllerBase {

    // Om lokaal te testen dit commando gebruiken (localtunnelme moet geinstalleerd zijn op je computer)
    // lt --port 8888 --local-host local.chirojongenszwevezele.be
    // const DOMAIN = 'https://little-sloth-47.localtunnel.me';
    // Uit commentaar halen wanneer live:
     const DOMAIN = 'https://www.chirojongenszwevezele.be';

    /**
     * The module storage service.
     *
     * @var MollieSubmissionStorage
     */
    protected $mollieSubmissionStorage;

    /**
     * The Mollie gateway used for making API calls.
     *
     * @var \Mollie\Api\MollieApiClient
     */
    protected $mollieApi;

    public function __construct(MollieSubmissionStorage $mollieSubmissionStorage) {
        $this->mollieApi = new \Mollie\Api\MollieApiClient();
        $this->mollieApi->setApiKey('live_5pkUBVVHkTzePS2Dg5Kucbnb7QUVjR');
        $this->mollieSubmissionStorage = $mollieSubmissionStorage;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('mvi_webform_mollie.storage')
        );
    }


    public function startPayment(WebformSubmissionInterface $webform_submission, Request $request){
        $submission_id = $webform_submission->id();
        $created = $webform_submission->getCreatedTime();
        $referenceName = 'CHIROZWEVEZELE' . date('Ymd', $created) . '-' . $submission_id;

        $afdelingTarieven = [
            'sloeber' => 100,
            'speelclub' => 190.00,
            'rakker' => 190.00,
            'topper' => 190.00,
            'kerel' => 205.00,
            'aspi' => 205.00,
            'leider' => 145.00,
        ];

        /*
        $kortingDoorBoterkaarten = [
            '0_9' => +15,
            '10_49' => 0,
            '50_9999' => -15
        ];
        */

        // Indien oudere broer = ja. nog eens -15

        $submissionData = $webform_submission->getData();

        $afdelingTariefKey = $submissionData['department'];
        $afdelingTarief = $afdelingTarieven[$afdelingTariefKey];

        //$boterkaartKey = $submissionData['boterkaarten'];
        //$boterkaartKorting = $kortingDoorBoterkaarten[$boterkaartKey];

        $price = $afdelingTarief /*+ $boterkaartKorting*/;

        if(key_exists('brother', $submissionData)){
            $brotherValue = $submissionData['brother'][0];
            if($brotherValue == "1"){
                $price = $price - 10;
            }
        }

        $payment = $this->mollieApi->payments->create([
            "amount" => [
                "currency" => "EUR",
                "value" => (string)$price . ".00"
            ],
            "description" => $referenceName,
            "redirectUrl" => self::DOMAIN . "/betaling-return/" . $submission_id,
            "webhookUrl"  => self::DOMAIN . "/mollie/webhook",
        ]);

        // Get the Mollie remote id and store it with this submission.
        $this->mollieSubmissionStorage->setMollieRemoteId($submission_id,$payment->id);
        // Set the default Mollie status.
        $this->mollieSubmissionStorage->setMollieStatus($submission_id, MolliePaymentStatus::STATUS_OPEN);

        return new TrustedRedirectResponse($payment->getCheckoutUrl(), 303);
    }

    /**
     * Callback for commerce_mollie.checkout.mollie_return route.
     *
     * Cancelled payment is redirected to route: commerce_payment.checkout.cancel
     * Processed payment is redirected to route: commerce_payment.checkout.return
     * Non-processed payment get JsonResponse with an informative reload-message.
     */
    public function returnFromMollieMiddleware(Request $request, RouteMatchInterface $route_match) {
        /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
        $webform_submission_id = $route_match->getParameter('webform_submission');
        /** @var \Drupal\Core\Messenger\MessengerInterface $messenger */
        $messenger = \Drupal::messenger();

        // Payment failed
        if ($this->mollieSubmissionStorage->getMollieStatus($webform_submission_id) === MolliePaymentStatus::STATUS_FAILED) {
            $messenger->addWarning($this->t('De betaling is mislukt. Probeer het opnieuw.'));
        }
        if ($this->mollieSubmissionStorage->getMollieStatus($webform_submission_id) === MolliePaymentStatus::STATUS_EXPIRED) {
            $messenger->addWarning($this->t('De betaling is verlopen. Probeer het opnieuw.'));
        }
        // Payments that are cancelled go to the last page of the submission.
        $cancel_route_states = [
            MolliePaymentStatus::STATUS_CANCELED,
            MolliePaymentStatus::STATUS_FAILED,
            MolliePaymentStatus::STATUS_EXPIRED,
        ];
        if (in_array($this->mollieSubmissionStorage->getMollieStatus($webform_submission_id), $cancel_route_states, TRUE)) {

            $webform_submission = WebformSubmission::load($webform_submission_id);
            $url = '/webform/contact/confirmation?token=' . $webform_submission->getToken();
            $cancel_url = Url::fromUserInput($url);

            return new RedirectResponse($cancel_url->toString());
        }

        // Payments that are processed, always go to complete (for example paid).
        if ($this->mollieSubmissionStorage->getMollieStatus($webform_submission_id) == MolliePaymentStatus::STATUS_PAID) {
            $confirm_url = Url::fromUserInput('/betaling-voltooid');

            return new RedirectResponse($confirm_url->toString());
        }

        // If payment is not processed, show reload message.
        \Drupal::service('page_cache_kill_switch')->trigger();
        return [
            '#markup' => $this->t('We have not yet received the payment status from Mollie. <a href="/betaling-return/@sid">Refresh page</a>', ['@sid' => $webform_submission_id]),
            '#cache' => ['max-age' => 0],
        ];

    }


    /**
     * This hook is called on every payment status change in Mollie and gives the id of the payment in Mollie. When
     * this hook is called, you have to check the remote Mollie status and act on it.
     */
    public function webhook(Request $request) {

        /*
         * Handle status that Mollie returns.
         */
        $mollie_payment_remote_id = $request->get('id');

        // Load the submission by the remote mollie id.
        $submission = $this->mollieSubmissionStorage->loadByRemoteMollieId($mollie_payment_remote_id);

        // If no submission is found, do not proceed.
        if($submission == null){
            \Drupal::logger('mvi_webform_mollie')->critical('Mollie did not find a submission for ' . $mollie_payment_remote_id);
            return new JsonResponse();
        }

        // Evaluate the remote status of the Mollie payment.
        $mollie_payment_remote_object = $this->mollieApi->payments->get($mollie_payment_remote_id);

        $status = $mollie_payment_remote_object->status;

        $this->mollieSubmissionStorage->setMollieStatus($submission->id(), $status);
        \Drupal::logger('mvi_webform_mollie')->critical('Succesfully changed Mollie status on submission with ID ' . $submission->id());

        // Return empty response with 200 status code.
        return new JsonResponse();
    }

}
