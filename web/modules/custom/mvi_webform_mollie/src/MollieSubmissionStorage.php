<?php

namespace Drupal\mvi_webform_mollie;

use Drupal\Core\Database\Connection;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Class MollieSubmissionStorage
 *
 * @package Drupal\mvi_webform_mollie
 */
class MollieSubmissionStorage implements mollieSubmissionStorageInterface{

    protected $database;

    /**
     * @param \Drupal\Core\Database\Connection $connection
     */
    public function __construct(Connection $database) {
        $this->database = $database;
    }

    /**
     * Get the Mollie status for the given webform submission
     *
     * @param int|mixed $sid
     *   Submission id.
     */
    public function getMollieStatus($sid) {
        $query = $this->database->select('webform_submission', 'ws')
            ->fields('ws', ['mollie_status'])
            ->condition('sid', $sid);

        $result = $query->execute()->fetchCol();

        if (!empty($result)) {
            return $result[0];
        }

        return 0;
    }

    /**
     * Set the Mollie status for the given webform submission
     *
     * @param int|mixed $sid
     *   Submission id.
     */
    public function setMollieStatus($sid, $status) {
        $this->database->update('webform_submission')
            ->fields(['mollie_status' => $status])
            ->condition('sid', $sid)
            ->execute()
        ;
    }

    /**
     * Get the Mollie remote id for the given webform submission
     *
     * @param int|mixed $sid
     *   Submission id.
     */
    public function getMollieRemoteId($sid) {
        $query = $this->database->select('webform_submission', 'ws')
            ->fields('ws', ['mollie_remote_id'])
            ->condition('sid', $sid);

        $result = $query->execute()->fetchCol();

        if (!empty($result)) {
            return $result[0];
        }

        return 0;
    }

    /**
     * Get the Webform submission by remote id
     *
     * @param int|mixed $sid
     *   Submission id.
     */
    public function loadByRemoteMollieId($remote_id) {
        $query = $this->database->select('webform_submission', 'ws')
            ->fields('ws', ['sid'])
            ->condition('mollie_remote_id', $remote_id);

        $result = $query->execute()->fetchCol();

        if (!empty($result)) {
            $submission = WebformSubmission::load($result[0]);
            return $submission;
        }

        return 0;
    }

    /**
     * Set the Mollie status for the given webform submission
     *
     * @param int|mixed $sid
     *   Submission id.
     * @param int|mixed $sid
     *   Remote id.
     */
    public function setMollieRemoteId($sid, $remoteId) {
        $this->database->update('webform_submission')
            ->fields(['mollie_remote_id' => $remoteId])
            ->condition('sid', $sid)
            ->execute()
        ;
    }
}