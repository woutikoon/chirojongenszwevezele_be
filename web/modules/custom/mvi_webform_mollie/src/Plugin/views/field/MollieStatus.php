<?php

namespace Drupal\mvi_webform_mollie\Plugin\views\field;

use Drupal\mvi_webform_mollie\MollieSubmissionStorageInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Shows the Mollie payment status
 *
 * @ViewsField("mvi_webform_mollie_remote_status")
 */
class MollieStatus extends FieldPluginBase {

    use UncacheableFieldHandlerTrait;

    /**
     * The Mollie submission storage.
     *
     * @var MollieSubmissionStorageInterface
     */
    protected $mollieSubmissionStorage;


    public function __construct(array $configuration, $plugin_id, $plugin_definition, MollieSubmissionStorageInterface $mollieSubmissionStorage) {
        parent::__construct($configuration, $plugin_id, $plugin_definition);

        $this->mollieSubmissionStorage = $mollieSubmissionStorage;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('mvi_webform_mollie.storage')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function clickSortable() {
        return TRUE;
    }

    /**
     * {@inheritdoc}
     */
    public function getValue(ResultRow $row, $field = NULL) {
        $webformSubmissionItem = $this->getEntity($this->view->result[$row->index]);

        $webformSubmissionId = $webformSubmissionItem->id();

        $mollieStatus = $this->mollieSubmissionStorage->getMollieStatus($webformSubmissionId);

        if($mollieStatus) {
            return $mollieStatus;
        } else {
            return null;
        }
    }

    public function render(ResultRow $row) {
        $value = $this->getValue($row);

        return $this->sanitizeValue($value);
    }

    /**
     * {@inheritdoc}
     */
    public function query() {
        // Do nothing.
    }

}
