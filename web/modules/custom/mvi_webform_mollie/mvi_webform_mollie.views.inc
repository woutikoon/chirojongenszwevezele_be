<?php

/**
 * Implements hook_views_data_alter().
 */
function mvi_webform_mollie_views_data_alter(array &$data) {
  $data['webform_submission']['mvi_webform_mollie_remote_status'] = [
    'title' => t('Mollie remote status'),
    'field' => [
      'title' => t('Mollie remote status'),
      'help' => t('The remote payment status for this submission in Mollie.'),
      'id' => 'mvi_webform_mollie_remote_status',
    ],
  ];
}