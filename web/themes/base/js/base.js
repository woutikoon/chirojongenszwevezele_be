/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */


  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      //alert("I'm alive!");
      AOS.init();
    }
  };

  Drupal.behaviors.fixedmenu = {
    attach: function (context, settings){
      $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        var objectSelect = $(".view-mode-full");
        var objectPosition = objectSelect.offset().top;
        if( scroll > objectPosition ) {
          $('#header-wrapper').addClass('kzientwi');
        } else {
          $('#header-wrapper').removeClass('kzientwi');
        }
      });
    }
  };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.cookies = {
        attach: function (context, settings) {
            $(document).ready(function() {
                $('.decline-button').on('click', function(){
                    $('#sliding-popup').remove();
                });
            });
        }
    };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.popUp = {
        attach: function (context, settings) {
            $(context).find('#block-popup').once('ifPopUp').each(function () {
                var cookiePopUpCheck = $.cookie("popUp-check");

                function checkCookie() {
                    cookiePopUpCheck = $.cookie("popUp-check", 1, {expires: 2});
                }
                if (!cookiePopUpCheck) {
                    $('#block-popup').foundation('open');
                }

                $('#close-pop-up').on('click', function () {
                    $("#block-popup").on("closed.zf.reveal", function (e) {
                        checkCookie();
                    });
                    $('#block-popup').foundation('close');
                });
            });
        }
    }

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.slides = {
        attach: function (context, settings) {
            $(context).find('.field-name-field-slides.swiper-container').once('ifLatestSlider').each(function (){
                var slides = new Swiper ('.field-name-field-slides.swiper-container', {
                    autoplay: true,
                    loop: true,
                    effect: 'fade',
                    pagination: {
                        el: '.swiper-pagination',
                        type: 'bullets',
                        clickable: true,
                    },
                });
            });
        }
    };

    /**
     * offCanvas behaviors
     */
    Drupal.behaviors.offCanvas = {
        attach: function (context, settings) {
            $(context).find('#offCanvasRightOverlap').once('ifRightCanvas').each(function () {
                $menuButton = $('#menuButton');
                $(this).on("opened.zf.offcanvas", function(e){
                    $menuButton.addClass('is-active');
                });
                $(this).on("closed.zf.offcanvas", function(e){
                    $menuButton.removeClass('is-active');
                });
            });
        }
    };

})(jQuery, Drupal);
